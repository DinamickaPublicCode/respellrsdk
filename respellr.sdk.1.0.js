/**
 * @typedef {string} linkToGoogleDoc
 * @typedef {Object} Messages
 * @property {string} CLOSE_ALL_MODALS
 * @property {string} SKIP_IT
 * @property {string} IS_GAME_MODAL_OPEN_TOGGLE
 * @property {string} WORD_WIN
 * @property {string} DICTATION
 * @property {string} CHALLENGE_WORD_INDEX
 * @property {string} IS_ALL_GAMES_FINISHED
 * @property {string} FINISH_GAME
 * @property {string} START_GAME
 * @property {string} GET_AVATAR_BUILDER_DATA
 * @property {string} SET_AVATAR_BUILDER_DATA
 * @property {string} AVATAR_BUILDER_LOADED
 */

/**
 * A constant that holds the URL for Google Docs
 * @type {linkToGoogleDoc}
 */
const linkToGoogleDoc = "https://docs.google.com/";

/**
 * Mock data for test mode
 */

const mockDictation = {
    "type": "dictation:response",
    "dictation": {
        "id": 3778,
        "dictation": [
            "Tihs",
            "is",
            "a",
            "test",
            "textt"
        ],
        "findWordsByUserInChallengeWords": [{
                "wordId": 1635,
                "userId": 3778,
                "wordTemp": "This",
                "lastTimeMisspelled": "2023-07-24T17:58:24.732Z",
                "lastTimeMastered": null,
                "misspelling": "Tihs",
                "priceOfWord": 0,
                "numberOfMisspelling": 3,
                "numberOfCorrect": 0,
                "numberOfLetters": 4,
                "numberOfMistakeLetters": 2,
                "inSentenceLastTime": " Tihs is a test textt",
                "wordIndexInSentence": 1,
                "isMastered": false,
                "numberOfWin": 0,
                "numberOfLose": 0,
                "dictationId": "1ptJWfhZPK6aZwRe1bibIOErbf5du4NwevIVmfcBKlBs",
                "points": 100,
                "createdAt": "2023-07-24T17:55:07.645Z",
                "updatedAt": "2023-07-24T17:58:24.732Z",
                "id": 11181
            },
            {
                "wordId": 1073,
                "userId": 3778,
                "wordTemp": "text",
                "lastTimeMisspelled": "2023-07-24T17:58:24.737Z",
                "lastTimeMastered": "2023-06-12T20:22:41.011Z",
                "misspelling": "textt",
                "priceOfWord": 0,
                "numberOfMisspelling": 19,
                "numberOfCorrect": 2,
                "numberOfLetters": 4,
                "numberOfMistakeLetters": 0,
                "inSentenceLastTime": " Tihs is a test textt",
                "wordIndexInSentence": 5,
                "isMastered": false,
                "numberOfWin": 0,
                "numberOfLose": 0,
                "dictationId": "1ptJWfhZPK6aZwRe1bibIOErbf5du4NwevIVmfcBKlBs",
                "points": 100,
                "createdAt": "2023-06-12T19:39:13.188Z",
                "updatedAt": "2023-07-24T17:58:24.738Z",
                "id": 9816
            }
        ],
        "numberOfErrorWordInDictation": 2,
        "numberOfWordInDictation": 5,
        "points": 835319
    },
    "id": 9946927
};
const mockStep1 = {
    "type": "challenge word index:response",
    "challengeWordIndex": 1,
    "id": 7526745
};
const mockGamesFinishedTrue = {
    "type": "is all games finished:response",
    "isAllGamesFinished": true,
    "id": 1069654
};
const mockGameFinishedTrue = {
    "type": "finish game:response",
    "isFinishGame": true,
    "id": 365553
};
const mockGamesFinishedFalse = {
    "type": "is all games finished:response",
    "isAllGamesFinished": false,
    "id": 3464424
};
const mockGameFinishedFalse = {
    "type": "finish game:response",
    "isFinishGame": false,
    "id": 8845553
};
const testGameSteps = 3;
let currTestGameStep = 0;

/** This logger is used for debugging */
class Logger {
    /**
     * The log method outputs a message to the web console.
     * @param {...unknown} args - The arguments to print to the console.
     */
    log(...args) {
        console.log(
            `[sdk][${new Date().toISOString()}][${this.name}][info]: `,
            ...args
        );
    }

    /**
     * The verbose method outputs a detailed message to the web console.
     * @param {...unknown} args - The arguments to print to the console.
     */
    verbose(...args) {
        console.log(
            `[sdk][${new Date().toISOString()}][${this.name}][verbose]: `,
            ...args
        );
    }

    /**
     * The error method outputs an error message to the web console.
     * @param {...unknown} args - The arguments to print to the console.
     */
    error(...args) {
        console.error(
            `[sdk][${new Date().toISOString()}][${this.name}][error]: `,
            ...args
        );
    }
}

/** This logger is used for debugging
 * @type {Logger}
 */
let logger = new Logger("respellr.sdk");

/**
 * Represents the messages used in the application.
 * Each key in the object represents a unique message.
 * @example
 * // Message for closing all modals
 * messages.CLOSE_ALL_MODALS // "close all modals"
 * // Message for skipping
 * messages.SKIP_IT // "skip it"
 * // Message for toggling game modal open status
 * messages.IS_GAME_MODAL_OPEN_TOGGLE // "is game modal open toggle"
 * // Message for word win
 * messages.WORD_WIN // "word win"
 * // Message for dictation
 * messages.DICTATION // "dictation"
 * // Message for challenge word index
 * messages.CHALLENGE_WORD_INDEX // "challenge word index"
 * // Message for checking if all games are finished
 * messages.IS_ALL_GAMES_FINISHED // "is all games finished"
 * // Message for finishing game
 * messages.FINISH_GAME // "finish game"
 * // Message for starting game
 * messages.START_GAME // "start game"
 * // Message for indicating game has been started
 * messages.GAME_STARTED // "game started"
 * // Message for getting avatar builder data
 * messages.GET_AVATAR_BUILDER_DATA // "get avatar builder data"
 * // Message for setting avatar builder data
 * messages.SET_AVATAR_BUILDER_DATA // "set avatar builder data"
 * // Message to indicate avatar builder has been loaded
 * messages.AVATAR_BUILDER_LOADED // "avatar builder loaded"
 * @type {Messages}
 */
const messages = {
    CLOSE_ALL_MODALS: "close all modals",
    SKIP_IT: "skip it",
    IS_GAME_MODAL_OPEN_TOGGLE: "is game modal open toggle",
    WORD_WIN: "word win",
    DICTATION: "dictation",
    CHALLENGE_WORD_INDEX: "challenge word index",
    IS_ALL_GAMES_FINISHED: "is all games finished",
    FINISH_GAME: "finish game",
    START_GAME: "start game",
    GAME_STARTED: "game started",
    GET_AVATAR_BUILDER_DATA: "get avatar builder data",
    SET_AVATAR_BUILDER_DATA: "set avatar builder data",
    AVATAR_BUILDER_LOADED: "avatar builder loaded",
};

/**
 * @param {sendMessageToGameTypes} message
 * @returns {void}
 */
const sendMessageToParent = (message) => {
    window.parent.postMessage(message, "*");
};

(function(window) {
    const RespellrSDK = {
        /**
         * Add listeners for game data
         * @returns {void}
         */
        addGameDataListeners() {
            const handler = (event) => {
                if (event.data.type === messages.START_GAME) {
                    RespellrSDK.getDictation().then((data) => {
                        window.RespellrSDK.currentDictation = data.dictation;
                        if (
                            window.RespellrSDK.gameStartPending &&
                            window.RespellrSDK.gameStartCallback
                        ) {
                            sendMessageToParent({
                                type: messages.GAME_STARTED
                            });
                            window.RespellrSDK.gameStartCallback(data.dictation);
                        }
                    })
                } else if (event.data.type === messages.SET_AVATAR_BUILDER_DATA) {
                    if (window.RespellrSDK.avatarBuilderDataCallback) {
                      window.RespellrSDK.avatarBuilderDataCallback(event.data.settings)
                    };
                } else {
                  return;
                }
            };

            window.addEventListener("message", handler);
        },
        /**
         * @param {(value: DictationType)=>void} callback
         * @returns {Promise<{object: handleWordWinItems}>}
         */
        startGame(callback) {
            if (window.respellrTestMode) {
                window.RespellrSDK.gameStartPending = true;
                window.RespellrSDK.gameStartCallback = callback;
                RespellrSDK.startTestGame();
            } else {
                if (window.RespellrSDK.currentDictation) {
                    sendMessageToParent({
                        type: messages.GAME_STARTED
                    });
                    callback(window.RespellrSDK.currentDictation);
                } else {
                    window.RespellrSDK.gameStartPending = true;
                    window.RespellrSDK.gameStartCallback = callback;
                }
            }
        },
        /**
         * @returns {void}
         */
        closeGame() {

            if (window.RespellrSDK.currentDictation) delete window.RespellrSDK.currentDictation;
            if (window.RespellrSDK.gameStartPending) delete window.RespellrSDK.gameStartPending;
            if (window.RespellrSDK.gameStartCallback) delete window.RespellrSDK.gameStartCallback;

            RespellrSDK.closeAllModals();
        },
        /**
         * @returns {void}
         */
        finishGame() {

            if (window.RespellrSDK.currentDictation) delete window.RespellrSDK.currentDictation;
            if (window.RespellrSDK.gameStartPending) delete window.RespellrSDK.gameStartPending;
            if (window.RespellrSDK.gameStartCallback) delete window.RespellrSDK.gameStartCallback;

            RespellrSDK.isGameModalOpenToggle(true);
        },
        /**
         * @returns {void}
         */
        closeAllModals() {
            sendMessageToParent({
                type: messages.CLOSE_ALL_MODALS,
            });
        },
        /**
         * @param {DictationType} dictation
         * @returns {void}
         */
        skipIt(dictation) {
            sendMessageToParent({
                type: messages.SKIP_IT,
                dictation,
            });
        },
        /**
         * @param {boolean} isGameModalOpenToggle
         * @returns {void}
         */
        isGameModalOpenToggle(isGameModalOpenToggle) {
            sendMessageToParent({
                type: messages.IS_GAME_MODAL_OPEN_TOGGLE,
                isGameModalOpenToggle,
            });
        },
        /**
         * @param {handleWordWinItems} wordWinItems
         * @returns {void}
         */
        wordWin(wordWinItems) {
            sendMessageToParent({
                type: messages.WORD_WIN,
                wordWinItems,
            });
        },
        /**
         * @returns {Promise<{object: handleWordWinItems}>}
         */
        getDictation() {
            const id = Math.round(Math.random() * 10000000);

            return new Promise((resolve) => {
                if (window.respellrTestMode) {
                    resolve(mockDictation);
                };
                sendMessageToParent({
                    type: messages.DICTATION,
                    id,
                });

                const handler = (event) => {
                    if (
                        event.data.type === `${messages.DICTATION}:response` &&
                        event.data.id === id
                    ) {
                        resolve(event.data);
                        window.removeEventListener("message", handler);
                    } else {
                        return;
                    }
                };

                window.addEventListener("message", handler);
            });
        },
        /**
         * @returns {Promise<{dictation: DictationType}>}
         */
        getChallengeWordIndex() {
            const id = Math.round(Math.random() * 10000000);

            return new Promise((resolve) => {
                if (window.respellrTestMode) {
                    currTestGameStep += 1;
                    resolve(mockStep1);
                }
                sendMessageToParent({
                    type: messages.CHALLENGE_WORD_INDEX,
                    id,
                });

                const handler = (event) => {
                    if (
                        event.data.type === `${messages.CHALLENGE_WORD_INDEX}:response` &&
                        event.data.id === id
                    ) {
                        resolve(event.data);
                        window.removeEventListener("message", handler);
                    } else {
                        return;
                    }
                };

                window.addEventListener("message", handler);
            });
        },
        /**
         * @returns {Promise<{challengeWordIndex: number}>}
         */
        getIsAllGamesFinished() {
            const id = Math.round(Math.random() * 10000000);

            return new Promise((resolve) => {
                if (window.respellrTestMode) {
                    if (currTestGameStep === testGameSteps) {
                        resolve(mockGamesFinishedTrue);
                    } else {
                        resolve(mockGamesFinishedFalse);
                    }

                }
                sendMessageToParent({
                    type: messages.IS_ALL_GAMES_FINISHED,
                    id,
                });

                const handler = (event) => {
                    if (
                        event.data.type === `${messages.IS_ALL_GAMES_FINISHED}:response` &&
                        event.data.id === id
                    ) {
                        resolve(event.data);
                        window.removeEventListener("message", handler);
                    } else {
                        return;
                    }
                };

                window.addEventListener("message", handler);
            });
        },
        /**
         * @returns {Promise<{isFinishGame: boolean}>}
         */
        getFinishGame() {
            const id = Math.round(Math.random() * 10000000);

            return new Promise((resolve) => {
                if (window.respellrTestMode) {
                    if (currTestGameStep === testGameSteps) {
                        resolve(mockGameFinishedTrue);
                    } else {
                        resolve(mockGameFinishedFalse);
                    }
                }
                sendMessageToParent({
                    type: messages.FINISH_GAME,
                    id,
                });

                const handler = (event) => {
                    if (
                        event.data.type === `${messages.FINISH_GAME}:response` &&
                        event.data.id === id
                    ) {
                        resolve(event.data);
                        window.removeEventListener("message", handler);
                    } else {
                        return;
                    }
                };

                window.addEventListener("message", handler);
            });
        },
        /**
         * @param {sendMessageToGameTypes} sendMessageToGameTypes
         * @returns {void}
         */
        sendMessageToGame({ data, iframeDiv }) {
            iframeDiv.contentWindow.postMessage(data, "*");
        },
        /**
         * Set test mode
         * @param {boolean} testMode
         * @returns {void}
         */
        setTestMode(testMode) {
            window.respellrTestMode = testMode;
        },
        /**
         * Start test game (only works with test mode enabled)
         * @returns {void}
         */
        startTestGame() {
            if (window.respellrTestMode) {
                currTestGameStep = 1;
                window.postMessage({
                    type: messages.START_GAME
                }, '*')
            }
        },
        /**
         * Export data from avatar builder (used to pass avatar builder results data to app/extension)
         * @param {string} imageData - base64 encoded image data
         * @param {string} settings - json containing avatar builder config options for current image
         * @returns {void}
         */
        exportAvatarBuilderData(imageData, settings) {
          sendMessageToParent({
              type: messages.GET_AVATAR_BUILDER_DATA,
              data: {
                imageData,
                settings
              }
          });
        },
        /**
        Indicate that avatar builder has been loaded and initialized
        */
        sendAvatarBuilderLoadedEvent() {
          sendMessageToParent({
              type: messages.AVATAR_BUILDER_LOADED,
          });
        },
        /**
         * Import data to avatar builder from app/extension
         * @param {string} settings - json containing avatar builder config options for current image
         * @param {HTMLElement} iframtDiv - dom element containing the iframe where avatar builder is embedded
         * @returns {void}
         */
        sendAvatarBuilderDataToBuilder(settings, iframeDiv) {
          RespellrSDK.sendMessageToGame({
            data: {
              type: messages.SET_AVATAR_BUILDER_DATA,
              settings
            },
            iframeDiv
          });
        },
        /**
         * subscribe to avatar builder data
         * @callback callback - callback that will be called when avatar builder data is received from the app it's embedded in
         * @returns {void}
         */
        subscribeOnAvatarBuilderData(callback) {
          window.RespellrSDK.avatarBuilderDataCallback = callback;
        },
    };

    window.RespellrSDK = RespellrSDK;
    RespellrSDK.addGameDataListeners();
})(window);
