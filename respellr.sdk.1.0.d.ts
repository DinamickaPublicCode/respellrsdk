//v1
declare const linkToGoogleDoc: number;

/** This logger is used for debugging */
declare class Logger {
  log(...args: unknown[]): void;
  verbose(...args: unknown[]): void;
  error(...args: unknown[]): void;
}

declare let logger: Logger;

declare namespace messages {
  let CLOSE_ALL_MODALS: number;
  let SKIP_IT: string;
  let IS_GAME_MODAL_OPEN_TOGGLE: string;
  let WORD_WIN: string;
  let DICTATION: string;
  let CHALLENGE_WORD_INDEX: string;
  let IS_ALL_GAMES_FINISHED: string;
  let FINISH_GAME: string;
  let START_GAME: string;
  let GAME_STARTED: string;
  let GET_AVATAR_BUILDER_DATA: string;
  let AVATAR_BUILDER_LOADED: string; 
}

type Word = {
  wordId: number;
  userId: number;
  wordTemp: string;
  lastTimeMisspelled: string;
  lastTimeMastered: string;
  misspelling: string;
  priceOfWord: number;
  numberOfMisspelling: number;
  numberOfCorrect: number;
  numberOfLetters: number;
  numberOfMistakeLetters: number;
  inSentenceLastTime: string;
  wordIndexInSentence: number;
  isMastered: false;
  numberOfWin: number;
  numberOfLose: number;
  dictationId: string;
  points: number;
  createdAt: string;
  updatedAt: string;
  id: number;
};

type DictationType = {
  id: number;
  dictation: string[];
  findWordsByUserInChallengeWords: Word[];
  numberOfErrorWordInDictation: number;
  numberOfWordInDictation: number;
  points: number;
};

type handleWordWinItems = {
  isWin: boolean;
  challengeWord_id: number;
  numberOfmistakeLetters?: number;
  dictation: DictationType;
};

type senMessageToParentTypes = {
  type: typeof messages;
  dictation?: DictationType;
  isGameModalOpenToggle?: boolean;
  wordWinItems: handleWordWinItems;
  id?: number;
};

declare function senMessageToParent(message: senMessageToParentTypes): void;

type sendMessageToGameTypes = {
  data: {
    id?: number;
    type: string;
    dictation?: DictationType;
    challengeWordIndex?: number;
    isAllGamesFinished?: boolean;
    isFinishGame?: boolean;
  };
  iframeDiv: HTMLElement | null;
};

declare global {
  interface Window {
    RespellrSDK: {
      startGame: (callback: (value: DictationType)=>void) => void;
      closeGame: () => void;
      finishGame: () => void;
      closeAllModals: () => void;
      skipIt: (dictation: DictationType) => void;
      isGameModalOpenToggle: (value: boolean) => void;
      wordWin: (object: handleWordWinItems) => void;
      getChallengeWordIndex: () => Promise<{ challengeWordIndex: number }>;
      getIsAllGamesFinished: () => Promise<{ isAllGamesFinished: boolean }>;
      getFinishGame: () => Promise<{ isFinishGame: boolean }>;
      getDictation: () => Promise<{ dictation: DictationType }>;
      sendMessageToGame: (values: sendMessageToGameTypes) => void;
    };
  }
}

export {};
