# how to use RespellrSDK

## Connecting the sdk

Load the sdk script
`<script src="https://s3.us-west-2.amazonaws.com/games-dev.respellr.com/respellr.sdk.1.0.js"></script>`

If the script is loaded correctly, it exposes a global variable `RespellrSDK` which is available from the window object on the browser environment.

## Test mode

The sdk offers a test mode which should be used for development purposes.
To enable it, use the following code:

```
RespellrSDK.setTestMode(true)
```

When enabled, sdk methods send mock data imitating the interaction with browser extension (& server).

## Game flow

In order to initalize the game the app must first subscribe to the game start event using the following code:

```
RespellrSDK.startGame((data: DictationType)=> {
  ...
})
```

The callback may fire multiple times (to start new game sessions), therefore it's important to use it to clean up the data from the previous game, reset app state to be able to relaunch the game.

Data type description of the data received in the callback:

```
type DictationType {
  id: number;
  dictation: string[];
  findWordsByUserInChallengeWords: Word[]; // array of words to be used in the game
  numberOfErrorWordInDictation: number; // number of misspelled words in dictation
  numberOfWordInDictation: number; // number of words in dictation
  points: number; // points user received for the dictation (without including the points that he can earn in the game)
}

type Word {
  wordId: number; // the id of word in db
  userId: number; // the id of user in db
  wordTemp: string;
  lastTimeMisspelled: string;
  lastTimeMastered: string;
  misspelling: string; // the text of the word with spelling error
  priceOfWord: number;
  numberOfMisspelling: number;
  numberOfCorrect: number;
  numberOfLetters: number; // number of letters in word
  numberOfMistakeLetters: number; // number of letters which are incorrect
  inSentenceLastTime: string; // the text of the sentence where the word was last encountered in dictation
  wordIndexInSentence: number; // the index of word in sentence (see above)
  isMastered: boolean;
  numberOfWin: number; // number of times user successfully completed a game with this word
  numberOfLose: number; // number of times user failed to complete a game with this word
  dictationId: string; // the id of the google doc the word was taken from
  points: number; // points that the user already received for "fixing" this word in games
  createdAt: string;
  updatedAt: string;
  id: number; // the id of the challenge word in db
}
```

Refer to `example-dictation-1.json` file to see full example of dictation data structure. (Also see the important note regarding dictation data structure at the end of the document).

The data that is required for the game is found in the `findWordsByUserInChallengeWords` field. This field contains an array of words with spelling mistakes that are to be used in the game.

The game may consist of a few identical or different steps. Each step involves working on one of the misspelled words from the `findWordsByUserInChallengeWords` array. In order to complete a game step the user must write the word, complete a letter puzzle with its letters or take some other action, the goal of which is to indicate that the user understands how to spell the word correctly.

Also the user has to have the option of skipping a game step (in which case he simply advances to the next game step).

The order of words in the game must be exactly as it is in the `findWordsByUserInChallengeWords` array. For example, game shouldn't be launched starting from the 2nd element of the array etc.

If the user successfully completed a game step, the following code should be used to advance to the next step:

```
RespellrSDK.wordWin(wordWin: WordWinType);

type WordWinType {
  isWin: boolean; //always true
  challengeWord_id: number; // field id from findWordsByUserInChallengeWords
  dictation: DictationType;
}
```

The user has to keep redoing the task for a game step until he either completes it successfully or skips it.

In order to skip a game step and andvance to the next game step the following code should be used:

```
RespellrSDK.skipIt(dictation);
```

When after calling either `RespellrSDK.wordWin` or `RespellrSDK.skipIt` there are no more words left in the `findWordsByUserInChallengeWords` array, a "Finish game" button should be displayed and the following method called upon it getting clicked:

```
RespellrSDK.finishGame();
```

The user must have an option to close the game at any point in time. For this purpose the game modal has to have a close button, upon clicking which the following method should be called:

```
RespellrSDK.closeGame();
```

# Important note regarding Dictation data structure

The games must be able to handle the case where the game is started based on the array of correctly spelled words as well. To see example data structure of the dication object that will be passed to the game in this case refer to the `example-dictation-2.json` file.

# how to hand over game code

1. Provide game name (will be shown in ReSpellr extension for game selection).
2. Provide game picture (will be shown in ReSpellr extension for game selection) - png or jpg, minimum size 180 x 78 px.
3. Disable test mode in sdk.
4. Provide a zip archive containing the build of the game (not the source code). Build should work in the browser environment.



# Avatar Builder integration

RespellrSDK offers 2 methods for interacting with the avatar builder application.

### detecting that avatar builder is loaded and ready to accept data
The method
```
RespellrSDK.sendAvatarBuilderLoadedEvent
```
should be called from the avatar builder when it is initialized in order to let the app/extension where it's embedded know that the avatar builder has loaded (can't use iframe onload event for this since unity app requires addtitional time to get running).

### exporting data from avatar builder into the app it's embedded in

The method that should be called from the code of the avatar builder in order to send data to the app it's embedded in is
```
RespellrSDK.exportAvatarBuilderData(imageData, settings)
```
where `imageData` is a string of base64 encoded image data to be exported and `settings` are the json object containing avatar builder image config.

### importing data into the avatar builder

The json object containing avatar builder config can be imported into avatar builder the following way:


call the method
```
RespellrSDK.subscribeOnAvatarBuilderData(callback)
```
when avatar builder is initialized. The callback that is passed as an argument to it will be fired when the json config data is passed to avatar builder from the app it's embedded in.  

### passing data from browser extension / app where avatar builder is embedded into avatar builder

The method

```
RespellrSDK.sendAvatarBuilderDataToBuilder(settings, iframeDiv)
```

should be called from the browser extension / app where avatar builder is embedded in order to pass data into the avatar builder. The `settings` param passed as an argument should contain the json config object for the avatar builder. The `iframeDiv` param is a HTML Element container of the iframe where the avatar builder app is embedded. NOTE: this method should be called from the app/extension, not avatar builder.

### avatar builder events

```
GET_AVATAR_BUILDER_DATA
```
this even is fired when avatar builder data is received in the app/extension the builder is embedded in

```
AVATAR_BUILDER_LOADED
```
this event is fired in the app/extension the builder is embedded in when the avatar builder is initialized 
